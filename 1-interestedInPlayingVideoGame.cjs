let givenObject = require('./1-users.cjs');

const intrestedPlayerInVideoGame = Object.entries(givenObject).filter((userData) => {
    if (userData[1].hasOwnProperty("interests")) {
        let interst = userData[1]["interests"];
        if (interst[0].includes("Video Games")) {
            return userData;
        }
    } else if (userData[1].hasOwnProperty("interest")) {
        let interst = userData[1]["interest"];
        if (interst[0].includes("Video Games")) {
            return userData;
        }
    }
}).map((userName) => userName[0]);

console.log(intrestedPlayerInVideoGame);
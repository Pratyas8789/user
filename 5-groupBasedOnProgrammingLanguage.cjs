let givenobject = require('./1-users.cjs');

const groupUserBasedOnProgramming = Object.entries(givenobject).reduce((acc, userData) => {
    for (let key in acc) {
        if (userData[1]["desgination"].includes(key)) {
            acc[key].push(userData[0]);
        }
    }
    return acc;
}, {
    "Golang": [],
    "Javascript": [],
    "Python": []
});

console.log(groupUserBasedOnProgramming);